/*
 * Copyright 2018-2019 Autoware Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/synchronizer.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_ros/transforms.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf2_eigen/tf2_eigen.h>
#include <yaml-cpp/yaml.h>
#include <chrono>
#include <omp.h>
class PointsConcatFilter
{
public:
  PointsConcatFilter();

private:
  typedef pcl::PointXYZI PointT;
  typedef pcl::PointCloud<PointT> PointCloudT;
  typedef sensor_msgs::PointCloud2 PointCloudMsgT;
  typedef message_filters::sync_policies::ApproximateTime<PointCloudMsgT, PointCloudMsgT, PointCloudMsgT,
                                                          PointCloudMsgT, PointCloudMsgT, PointCloudMsgT,
                                                          PointCloudMsgT, PointCloudMsgT>
      SyncPolicyT;

  ros::NodeHandle node_handle_, private_node_handle_;
  message_filters::Subscriber<PointCloudMsgT> *cloud_subscribers_[8];
  message_filters::Synchronizer<SyncPolicyT> *cloud_synchronizer_;
  ros::Subscriber config_subscriber_;
  ros::Publisher cloud_publisher_;
  tf::TransformListener tf_listener_;

  tf2_ros::TransformListener          tf2_listener_;/*!< Stores the Transform Listener */
  tf2_ros::Buffer                     tf2_buffer_;/*!< TF buffer to query the TF tree */
  geometry_msgs::Transform            geom_cloud_transforms_[8];
  bool                                ready_cloud_transforms_[8];
  Eigen::Matrix<float, 4, 4>          matrix_cloud_transforms_[8];;
 
  size_t input_topics_size_;
  std::string input_topics_;
  std::string output_frame_id_;

  void pointcloud_callback(const PointCloudMsgT::ConstPtr &msg1, const PointCloudMsgT::ConstPtr &msg2,
                           const PointCloudMsgT::ConstPtr &msg3, const PointCloudMsgT::ConstPtr &msg4,
                           const PointCloudMsgT::ConstPtr &msg5, const PointCloudMsgT::ConstPtr &msg6,
                           const PointCloudMsgT::ConstPtr &msg7, const PointCloudMsgT::ConstPtr &msg8);

  bool
  FindTransform(const std::string &in_target_frame, const std::string &in_source_frame, geometry_msgs::Transform& out_transform);

  bool
  str_replace(std::string& str, const std::string& str_from, const std::string& str_to);

};


// tf_eigen.h only supports eigen double matrix (affine and isometry)
void geomToEigen4f(const geometry_msgs::Transform & tf, Eigen::Matrix<float, 4,4> & m)
{
  Eigen::Quaterniond Q;
  tf2::fromMsg(tf.rotation, Q);
  Eigen::Matrix3f R = (Q.cast<float>()).toRotationMatrix();
  
  Eigen::Vector3d t_d;
  tf2::fromMsg(tf.translation, t_d);
  Eigen::Vector3f t = t_d.cast<float>(); 
  m.block<3,3>(0, 0) = R;
  m(0, 3) = t(0);
  m(1,3) = t(1);
  m(2,3) = t(2);
  for (int col = 0; col < 3; col++) {
    m(3,col) = 0.0f;
  }
  m(3,3) = 1.0f;

}

PointsConcatFilter::PointsConcatFilter() : node_handle_(), private_node_handle_("~"), tf_listener_(), tf2_listener_(tf2_buffer_)
{
  private_node_handle_.param("input_topics", input_topics_, std::string("[/points_alpha, /points_beta]"));
  private_node_handle_.param("output_frame_id", output_frame_id_, std::string("velodyne"));

  YAML::Node topics = YAML::Load(input_topics_);
  input_topics_size_ = topics.size();
  omp_set_num_threads(input_topics_size_);
  Eigen::setNbThreads(input_topics_size_);

  if (input_topics_size_ < 2 || 8 < input_topics_size_)
  {
    ROS_ERROR("The size of input_topics must be between 2 and 8");
    ros::shutdown();
  }
  for (size_t i = 0; i < 8; ++i)
  {
    ready_cloud_transforms_[i] = false;
  
    if (i < input_topics_size_)
      {
	cloud_subscribers_[i] =
          new message_filters::Subscriber<PointCloudMsgT>(node_handle_, topics[i].as<std::string>(), 1);
      }
    else
      {
	cloud_subscribers_[i] =
	  new message_filters::Subscriber<PointCloudMsgT>(node_handle_, topics[0].as<std::string>(), 1);
      }
      
  }
  cloud_synchronizer_ = new message_filters::Synchronizer<SyncPolicyT>(
      SyncPolicyT(10), *cloud_subscribers_[0], *cloud_subscribers_[1], *cloud_subscribers_[2], *cloud_subscribers_[3],
      *cloud_subscribers_[4], *cloud_subscribers_[5], *cloud_subscribers_[6], *cloud_subscribers_[7]);
  cloud_synchronizer_->registerCallback(
      boost::bind(&PointsConcatFilter::pointcloud_callback, this, _1, _2, _3, _4, _5, _6, _7, _8));
  cloud_publisher_ = node_handle_.advertise<PointCloudMsgT>("/points_concat", 1);
}

void PointsConcatFilter::pointcloud_callback(const PointCloudMsgT::ConstPtr &msg1, const PointCloudMsgT::ConstPtr &msg2,
                                             const PointCloudMsgT::ConstPtr &msg3, const PointCloudMsgT::ConstPtr &msg4,
                                             const PointCloudMsgT::ConstPtr &msg5, const PointCloudMsgT::ConstPtr &msg6,
                                             const PointCloudMsgT::ConstPtr &msg7, const PointCloudMsgT::ConstPtr &msg8)
{
  assert(2 <= input_topics_size_ && input_topics_size_ <= 8);

  PointCloudMsgT::ConstPtr msgs[8] = { msg1, msg2, msg3, msg4, msg5, msg6, msg7, msg8 };
  PointCloudT::Ptr cloud_sources[8];
  PointCloudT::Ptr cloud_concatenated(new PointCloudT);


  // transform points
  try
  {
    //auto start = std::chrono::high_resolution_clock::now();

    //obtain transforms once
#pragma omp parallel default(none) shared(ready_cloud_transforms_, matrix_cloud_transforms_, geom_cloud_transforms_, msgs)
  #pragma omp for

    for (size_t i = 0; i < input_topics_size_; ++i)
    {
      if (!ready_cloud_transforms_[i])
      {
        ready_cloud_transforms_[i] = FindTransform(output_frame_id_, msgs[i]->header.frame_id,
                                                   geom_cloud_transforms_[i]);
	geomToEigen4f(geom_cloud_transforms_[i], matrix_cloud_transforms_[i]);

      }
    }
#pragma omp parallel default(none) shared(ready_cloud_transforms_, matrix_cloud_transforms_, geom_cloud_transforms_, cloud_sources, msgs)
  #pragma omp for

    for (size_t i = 0; i < input_topics_size_; ++i)
    {
      // Note: If you use kinetic, you can directly receive messages as
      // PointCloutT.
      cloud_sources[i] = PointCloudT().makeShared();
      pcl::fromROSMsg(*msgs[i], *cloud_sources[i]);

      auto cloud_e_full = cloud_sources[i]->getMatrixXfMap();
      auto cloud_e = (cloud_e_full.topRows(4));

      cloud_e.noalias() = matrix_cloud_transforms_[i]*cloud_e;

    }
    // auto elapsed = std::chrono::high_resolution_clock::now() - start;
    // auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
    // ROS_INFO_STREAM("Concatenate Pointclouds [ms]: " << milliseconds);
  
  }
  catch (tf::TransformException &ex)
  {
    ROS_ERROR("%s", ex.what());
    return;
  }

  // merge points
  for (size_t i = 0; i < input_topics_size_; ++i)
  {
    if(ready_cloud_transforms_[i])
    {
      *cloud_concatenated += *cloud_sources[i];
    }
  }

  // publsh points
  cloud_concatenated->header = pcl_conversions::toPCL(msgs[0]->header);
  cloud_concatenated->header.frame_id = output_frame_id_;
  cloud_publisher_.publish(cloud_concatenated);
}

bool PointsConcatFilter::str_replace(std::string& str, const std::string& str_from, const std::string& str_to) {
  size_t start_pos = str.find(str_from);
  if(start_pos == std::string::npos)
    return false;
  str.replace(start_pos, str_from.length(), str_to);
  return true;
}

bool
PointsConcatFilter::FindTransform(const std::string &in_target_frame, const std::string &in_source_frame, geometry_msgs::Transform& out_transform)
{
  geometry_msgs::TransformStamped transform;

  std::string target_frame = in_target_frame;
  std::string source_frame = in_source_frame;
  PointsConcatFilter::str_replace(target_frame, "/", "");
  PointsConcatFilter::str_replace(source_frame, "/", "");
  try
  {
    transform = tf2_buffer_.lookupTransform(target_frame, source_frame, ros::Time(0));
    ROS_INFO("[%s] %s to %s TF obtained", ros::this_node::getName().c_str(), source_frame.c_str(), target_frame.c_str());
    out_transform = transform.transform;
    return true;
  }
  catch (tf2::TransformException &ex)
  {
    ROS_ERROR("[%s] %s . While getting transformation between %s and %s", ros::this_node::getName().c_str(), ex.what(),
              source_frame.c_str(), target_frame.c_str());
  }

  return false;
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "points_concat_filter");
  PointsConcatFilter node;
  ros::spin();
  return 0;
}
